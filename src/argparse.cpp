#include "argparse/argparse.h"

#include <algorithm>

const argparse::Blank ArgParse::NULL_VALUE{};

ArgParse::ArgParse(std::string name, std::string description)
    : _name{std::move(name)},
      _description{std::move(description)}
{

}

auto ArgParse::addArgument(
        std::string name,
        std::vector<std::string> &&flags,
        std::string_view help,
        argparse::Arg&& defaultValue,
        bool required,
        bool hasValue,
        bool multivalues,
        std::variant<StringType, IntType, DoubleType, LongType, BoolType> &&type
        ) -> Argument&
{
    return _arguments.emplace(
                std::move(name),
                Argument(
                    flags,
                    std::move(defaultValue),
                    std::string(help),
                    required,
                    hasValue,
                    multivalues,
                    std::move(type))).first->second;
}

void ArgParse::printHelp(std::ostream &os) const
{
    os << _name << ": " << _description << std::endl;
    os << "Usage: " << _name << ": " << std::endl;

    for (const auto & item: _arguments) {
        const auto & key = item.first;
        const auto & arg = item.second;
        os << "\t" << key << " type=" << (arg.type->*getType)() << ", (";
        for (auto const &flag: arg.flags) {
            os << flag << ", ";
        }
        os << ") : \t";
        os << "required = " << std::boolalpha << arg.required << ", ";

        if (arg.help.size() > 0) {
            os << "help: " << arg.help << " ";
        }
        os << std::endl;
    }
}

void ArgParse::parse(int argc, const char **argv)
{
    for (int i{1}; i < argc; i++) {
        if (argv[i][0] == '-') {
            std::string argName{argv[i]};
            bool unknown = true;
            for (auto& item: _arguments) {
                auto &arg = item.second;
                const auto &flags = arg.flags;
                auto found = std::find(
                            flags.cbegin(),
                            flags.cend(),
                            argName);
                if (found != flags.cend()) {
                    unknown = false;
                    if (not arg.multivalues) {
                        if (arg.isNotBool) {
                            try {
                                arg.arg.emplace(arg.arg.begin(), arg.tryParseString(argv[i+1]));
                            } catch (const std::runtime_error &ex) {
                                throw std::runtime_error("Invalid type in argument " + argName);
                            }
                        } else {
                            // it's bool
                            arg.arg[0] = true;
                        }
                        if (arg.action) {
                            arg.action();
                        }
                    } else {
                        try {
                            arg.arg.emplace_back(arg.tryParseString(argv[i+1]));
                        } catch (const std::runtime_error &ex) {
                            throw std::runtime_error("Invalid type in argument " + argName);
                        }
                    }
                }
            }
            if (unknown) {
                throw std::runtime_error("Unknown option " + argName);
            }
        }
    }

    for (const auto &item: _arguments) {
        const auto &arg = item.second;
        if (arg.required and arg.isNotBool and arg.arg.empty()) {
            throw std::runtime_error("The argument " + item.first + " is required!");
        }
    }
}

const std::vector<argparse::Arg> &ArgParse::get(const std::string &name) const
{
    return _arguments.at(name).arg;
}

bool ArgParse::hasValue(const std::string &name) const
{
    return _arguments.at(name).hasValue();
}


argparse::Arg ArgParse::Argument::tryParseString(std::string_view value)
{

    if (not (type->*fromString)(value)) {
        throw std::runtime_error("Invalid type received!");
    }
    return std::visit([](auto && v) ->decltype (auto) {
        return argparse::Arg(v.result);
    },
    type
    );
}

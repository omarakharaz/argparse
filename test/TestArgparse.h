#pragma once

#include "gtest/gtest.h"

#include "argparse/argparse.h"

#if (defined __cplusplus) && (__cplusplus >= 201700L)
#define GetVariant std::get_if
#else
#define GetVariant boost::get
#endif

using ::testing::Return;


class TestArgParse: public ::testing::Test
{
public:
    void SetUp() override
    {

    }
};


TEST_F(TestArgParse, simpleCmd)
{
    int argc{2};
    ArgParse argpa("command", "Description of command");
    auto &argument_help = argpa.addArgument(
                "help",
                {"-h", "--help"},
                "",
                false,
                false,
                false
                );
    argument_help.action = [&argpa](){argpa.printHelp(std::cout);};
    argpa.addArgument(
                "double",
                {"-d", "--double"},
                "Test parse double",
                ArgParse::NULL_VALUE,
                false,
                true,
                false,
                DoubleType()
        );
    try {
        const char *argv[2];
        argv[0] = "command";
        argv[1] = "-l";
        argpa.parse(argc, argv);

    }  catch (const std::runtime_error &ex) {
        std::cerr << "FIXME : "  << ex.what() << "\n";
    }
    {
        const char *argv[2];
        argv[0] = "command";
        argv[1] = "--help";
        argpa.parse(argc, argv);
        auto help = argpa.get("help")[0];
        EXPECT_TRUE(GetVariant<bool>(&help));
    }
    {
        const char *argv[2];
        argv[0] = "command";
        argv[1] = "-d";
        argv[2] = "245";
        argpa.parse(argc, argv);
        auto double_arg = argpa.get("double")[0];
        const auto *value = std::get_if<double>(&double_arg);
        EXPECT_EQ(*value, 245);
    }

    {
        const char *argv[2];
        argv[0] = "command";
        argv[1] = "-d";
        argv[2] = "ba245";
        EXPECT_THROW(argpa.parse(argc, argv), std::runtime_error);
    }
}

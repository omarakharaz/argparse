from os import environ
from conan import ConanFile
from conans import tools, CMake


def get_version():
    if 'CI_COMMIT_TAG' in environ:
        return '%s' %(environ['CI_COMMIT_TAG'])
    if 'CI_DEFAULT_BRANCH' in environ:
        return "%s_%s" %(environ['CI_DEFAULT_BRANCH'], environ['CI_COMMIT_BEFORE_SHA'])
    git = tools.Git()
    try:
        if git.get_tag():
            return "%s" % (git.get_tag())
        return "%s_%s" % (git.get_branch(), git.get_revision())
    except BaseException:
        return None


class ArgParseConan(ConanFile):
    name = "ArgParse"
    version = get_version()

    # Optional metadata
    license = "LGPL v3"
    author = "Omar AKHARAZ omar.akharaz@gmail.com"
    url = "http://gitlab.com/omarakharaz/argparse"
    description = "Argparse in c++"

    # Binary configuration
    settings = "cppstd", "os", "compiler", "build_type", "arch"
    options = {"shared": (True, False), "fPIC": (True, False), "build_test": (True, False)}
    default_options = {"shared": False, "fPIC": True, "build_test": True}

    generators = "cmake"

    scm = {
            "type": "git",
            "subfolder": ".",
            "url": "git@gitlab.com:omarakharaz/argparse.git",
            "revision": "auto",
        }

    requires = ()

    def build_requirements(self):
        self.build_requires("doxygen/1.9.2")
        if self.settings.build_type == "Debug":
            self.build_requires("kcov/38")
        if self.options.build_test:
            self.build_requires("gtest/1.11.0")

    def build(self):
        cmake_release = CMake(self, build_type="Release")
        cmake_release.configure()
        cmake_release.build()

        cmake_debug = CMake(self, build_type="Debug")
        cmake_debug.configure()
        cmake_debug.build()
        if self.settings.build_type == "Debug":
            self.run('cmake --build . --target coverage')
        else:
            self.run('ctest --verbose')

    def test(self):
        cmake = CMake(self)
        cmake.target(test)

    def package(self):
        cmake = CMake(self)
        cmake.install()

    def package_info(self):
        self.cpp_info.release.libs = ["argparse"]
        self.cpp_info.debug.libs = ["argparse_d"]

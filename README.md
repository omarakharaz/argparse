# ArgParse

Inspired by python's argparse, I created this free code to make parsing options easier in c++

## Requirement

### Build:

 - conan >= 1.46.2
 - cmake >= 3.18.4 
 - kcov
 - gtest


## Build and test

### With conan:

Download dependencies, generate the conan cmake file and create the build directories
```bash
conan install .
````
/!\ If the conan not found the binaries file of some dependencies, add option --build=missing

Build type Debug
```bash
conan install . --build missing -s build_type=Debug
```

Launch the compilation and the unit test
```bash
conan build .
```

### With CMake

Create the directory build
```bash
cmake -S . -B build -DCMAKE_BUILD_TYPE=Release -DEXECUTE_CONAN=ON -DBUILD_MISSING=ON -GNinja
```

Compile
```bash
cmake --build .
```

Execute unittest
```bash
cmake --build . --target test
```


#pragma once

#include <map>
#include <vector>
#include <string>
#include <iostream>
#include <functional>

#include <charconv>

#if (defined __cplusplus) && (__cplusplus >= 201700L)
    #include <variant>
#else
    #include <boost/variant/variant.hpp>
    #include <boost/variant/get.hpp>
#endif


namespace argparse {
#if (defined __cplusplus) && (__cplusplus >= 201700L)
using Blank = std::monostate;
using Arg = std::variant<Blank, int, double, long, std::string, bool>;
#define _vIndex(Variant) Variant.index()
#else
using Blank = boost::blank;
using Arg = boost::variant<Blank, int, double, std::string, bool>;
#define _vIndex(Variant) Variant.which()
};
#endif
}

struct StringType
{
    static constexpr const std::string_view Type{"string"};
    static constexpr const std::string_view getType() noexcept {
        return Type;
    }
    std::string result;
    bool fromString(std::string_view value) {
        result = value;
        return true;
    }
};

template<typename T>
bool _fromString(std::string_view value, T& result)
{
    auto [ptr, ec] { std::from_chars(value.data(), value.data() + value.size(), result) };
    if (ec == std::errc()) {
        return true;
    } else if (ec == std::errc::invalid_argument) {
        return false;
    } else if (ec == std::errc::result_out_of_range) {
        // Remote information..
        return false;
    }
    return false;
}

struct IntType
{
    static constexpr const std::string_view Type{"int"};
    static constexpr const std::string_view getType() noexcept {
        return Type;
    }
    int result;
    bool fromString(std::string_view value) {
        return _fromString(value, result);
    }
};


struct DoubleType
{
    static constexpr const std::string_view Type{"double"};
    static constexpr const std::string_view getType() noexcept {
        return Type;
    }
    double result;
    bool fromString(std::string_view value) {
        return _fromString(value, result);
    }
};

struct BoolType
{
    static constexpr const std::string_view Type{"bool"};
    static constexpr const std::string_view getType() noexcept {
        return Type;
    }
    bool result;
    bool fromString(std::string_view value) {
        if (value == "true") {
            result = true;
            return true;
        } else if (value == "false") {
            result = false;
            return true;
        } else {
            return false;
        }
        return false;
    }
};

struct LongType
{
    static constexpr const std::string_view Type{"long"};
    static constexpr const std::string_view getType() noexcept {
        return Type;
    }
    long result;
    bool fromString(std::string_view value) {
        return _fromString(value, result);
    }
};

using type_variant = std::variant<StringType, IntType, DoubleType, LongType, BoolType>;

template<typename F>
struct pseudo_method {
    F f;
    pseudo_method(F&& fin) noexcept: f(std::move(fin)) {}

    template<class Variant>
    friend decltype (auto) operator->*(Variant&& var, pseudo_method const & method) {
        return [&](auto&&... args) -> decltype (auto) {
            return std::visit(
                        [&](auto&& self)-> decltype (auto) {
                            return method.f(decltype (self)(self), decltype (args)(args)...);
                        },
                        std::forward<Variant>(var)
                    );
        };
    }
};

pseudo_method fromString = [](auto&& self, auto&&... args)-> decltype (auto) {
    return decltype(self)(self).fromString(decltype (args)(args)...);
};

pseudo_method getType = [](auto&& self) -> decltype (auto) {
    return decltype(self)(self).getType();
};

class ArgParse
{
private:

    struct Argument {
        std::vector<std::string> flags;
        std::vector<argparse::Arg> arg{};
        std::string help;
        bool required{false};
        bool isNotBool{true};
        bool multivalues{false};
        type_variant type{StringType()};
        std::function<void()> action;

        explicit Argument(
                std::vector<std::string> pFlags,
                argparse::Arg&& pArg,
                std::string pHelp,
                bool pRequired = false,
                bool pIsNotBool=false,
                bool pMultiValues=false,
                type_variant && pType = StringType()
                )
            : flags{std::move(pFlags)},
              help{std::move(pHelp)},
              required{pRequired},
              isNotBool{pIsNotBool},
              multivalues{pMultiValues},
              type{std::move(pType)}
        {
            if (_vIndex(pArg) != 0) {
                if (multivalues) {
                    throw std::runtime_error("multi values options can't have default value!");
                }
                arg.emplace_back(std::move(pArg));
            } else if (not isNotBool) {
                arg.emplace_back(false);
            }
        }

        bool hasValue() const
        {
           return not arg.empty();
        }

        argparse::Arg tryParseString(std::string_view value);
    };
public:
    static const argparse::Blank NULL_VALUE;

    explicit ArgParse(std::string name, std::string description);

    auto addArgument(
            std::string name,
            std::vector<std::string>&& flags,
            std::string_view help="",
            argparse::Arg&& defaultValue=NULL_VALUE,
            bool required=false,
            bool hasValue=true,
            bool multivalues=false,
            type_variant &&type = StringType()
            ) -> Argument&;

    void printHelp(std::ostream& os=std::cerr) const;

    void parse(int argc, const char **argv);

    const std::vector<argparse::Arg> &get(const std::string & name) const;

    bool hasValue(const std::string &name) const;

private:
    std::string _name;
    std::string _description;
    std::map<std::string, Argument> _arguments;

};
